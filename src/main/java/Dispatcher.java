import java.util.concurrent.Semaphore;
import java.util.PriorityQueue;

public class Dispatcher {

    private Semaphore semaforo;
    private PriorityQueue <Integer> colaDeLlamadas;

    public Dispatcher() {
        semaforo = new Semaphore(1);
        colaDeLlamadas = new PriorityQueue <Integer>();
    }

    public void dispatchCall(Integer numeroLlamada) {
        try {
            semaforo.acquire();
            colaDeLlamadas.add(numeroLlamada);
            semaforo.release();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public int finalizarLlamada() {
        int llamada = 0;
        try {
            if (hayLlamadasPendientes()) {
                semaforo.acquire();
                llamada = colaDeLlamadas.poll();
                semaforo.release();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return llamada;
    }

    public boolean hayLlamadasPendientes() {
        return colaDeLlamadas.size() > 0;
    }
}
