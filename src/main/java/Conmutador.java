import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Conmutador {

    int cantidadLlamadas;

    // se define que existira un numero finito y definido de empledos
    static final int OPRERADORES = 5;
    static final int SUPERVISORES = 2;
    static final int DIRECTORES = 1;

    static final int EMPLEADOS_DISPONIBLES = 8;

    public Conmutador(int cantidadLlamadas){
        this.cantidadLlamadas = cantidadLlamadas;
    }

    public void procesarLlamadas(){

        Dispatcher dispatcher = new Dispatcher();

        System.out.println(" se procesaran un total de " + cantidadLlamadas + " llamadas por un total de " + EMPLEADOS_DISPONIBLES + " empleados "  );

        // defino un poll de hilos que ejecutaran todas las tareas
        ExecutorService executor = Executors.newFixedThreadPool(10);

        Llamada[] v = new Llamada[cantidadLlamadas];
        for (int i = 0; i < cantidadLlamadas; i++) {
            v[i] = new Llamada(i + 1, dispatcher);
            executor.execute(v[i]);
        }
        // Creacion de hilos empleados que estaran disponibles para atender la llamada
        OpereradorTread[] operadores = new OpereradorTread[OPRERADORES];
        SupervisorTread[] supervisores = new SupervisorTread[SUPERVISORES];
        DirectorTread[] directores = new  DirectorTread[DIRECTORES];


        //lanzando hilo operadores
        for (int i = 0; i < OPRERADORES; i++) {
            operadores[i] = new OpereradorTread(i + 1, dispatcher);
            operadores[i].start();
        }

        //lanzando hilos supervisor
        for (int i = 0; i < SUPERVISORES; i++) {
            supervisores[i] = new SupervisorTread(i + 1, dispatcher);
            supervisores[i].start();
        }


        //lanzando hilos director
        for (int i = 0; i < DIRECTORES; i++) {
            directores[i] = new DirectorTread(i + 1, dispatcher);
            directores[i].start();
        }

        // Se espera a que terminen de atender los operadores
        for (int i = 0; i < OPRERADORES; i++) {
            try {
                operadores[i].join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        // Se espera a que terminen de atender los supervisores
        for (int i = 0; i < SUPERVISORES; i++) {
            try {
                supervisores[i].join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }


        // Se espera a que terminen de atender los directores
        for (int i = 0; i < DIRECTORES; i++) {
            try {
                directores[i].join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }


        // Se espera a que terminen de atender todas las llamadas ingresadas
        for (int i = 0; i < cantidadLlamadas; i++) {
            try {
                v[i].join();
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        // Se cierra el dispatcher
        System.out.println("el Dispatcher termino de atender todas las llamadas entrantes");

        //cuando se terminan de ejecutar todas las tareas, finaliza el servicio
        executor.shutdown();

    }

}
