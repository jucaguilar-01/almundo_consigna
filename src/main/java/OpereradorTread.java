public class OpereradorTread extends Thread  {

    private int identif;
    private Dispatcher dispatcher;

    public OpereradorTread(int identif, Dispatcher dispatcher){

        this.identif = identif;
        this.dispatcher = dispatcher;
    }

    public void run() {
        int duracionLlamada;
        int numeroTelefono;
        while (dispatcher.hayLlamadasPendientes()) {
            try {
                duracionLlamada = (int) (Math.random() * 10000 + 5000);
                numeroTelefono = dispatcher.finalizarLlamada();
                sleep(duracionLlamada);
                System.out.println("El operador " + identif + " ha atendido la llamana nro " + numeroTelefono + " en un tiempo de " + duracionLlamada + " milisegundos ");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
