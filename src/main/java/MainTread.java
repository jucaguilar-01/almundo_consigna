import java.util.Scanner;

public class MainTread {

    public static void main(String[] args) {

        System.out.println (" Por favor introduzca la cantidad de llamadas a procesar: ");
        String cantidadLlamadas = "";
        Scanner entradaEscaner = new Scanner (System.in);
        cantidadLlamadas = entradaEscaner.nextLine ();
        int nro = Integer.parseInt(cantidadLlamadas);

        Conmutador conmutador = new Conmutador(nro);
        conmutador.procesarLlamadas();
    }

}
