public class DirectorTread extends Thread  {

    private int identif;
    private Dispatcher dispatcher;

    public DirectorTread(int identif, Dispatcher dispatcher){

        this.identif = identif;
        this.dispatcher = dispatcher;
    }


    public void run() {
        int duracionLlamada;
        int numeroTelefono;
        while (dispatcher.hayLlamadasPendientes()) {
            try {
                duracionLlamada = (int) (Math.random() * 10000 + 5000);
                numeroTelefono = dispatcher.finalizarLlamada();
                sleep(duracionLlamada);
                System.out.println("El director " + identif + " ha atendido la llamana nro " + numeroTelefono + " en un tiempo de " + duracionLlamada + " milisegundos ");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
