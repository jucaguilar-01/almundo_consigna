public class Llamada extends Thread  {

    private int identif;
    private Dispatcher dispatcher;

    public Llamada(int identif, Dispatcher dispatcher) {
        this.identif = identif;
        this.dispatcher = dispatcher;
    }

    public void run() {
        dispatcher.dispatchCall(identif);
    }

}
