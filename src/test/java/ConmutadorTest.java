import org.junit.Test;
import static org.junit.Assert.*;

public class ConmutadorTest {

    /* este test verifica la ejecucion del maximo de hilos permitidos
     (existe un poll de 10 tread como maximo para procesar todas las tareas)
    */
    @Test
    public void hacer10LlamadasTest(){

        try {
            Conmutador conmutadorTest = new Conmutador(10);
            conmutadorTest.procesarLlamadas();
           } catch (Exception e){
            fail("ocurio un error durante la ejecucion del test = " + e.getLocalizedMessage() );
        }
    }

    /* este test verifica la ejecucion correcta de 15 llamadas, que excede el poll de hilos disponibles (10 tread)
*/
    @Test
    public void hacerMasDe10LlamadasTest(){

        try {
            Conmutador conmutadorTest = new Conmutador(15);
            conmutadorTest.procesarLlamadas();
        } catch (Exception e){
            fail("ocurio un error durante la ejecucion del test = " + e.getLocalizedMessage() );
        }
    }


}